$(document).ready(() => {
    $('.experience').click();
})

let selection;
$('.experience').click(() => {
    $('.location,.skill').css('border-bottom', 'none');
    $('.experience').css('border-bottom', '4px solid #E56553');
    $('.searchInput').val('');
    $('.searchInput').attr('placeholder', 'Enter your experience');
    selection = 'experience';
})

$('.location').click(() => {
    $('.experience,.skill').css('border-bottom', 'none');
    $('.location').css('border-bottom', '4px solid #E56553');
    $('.searchInput').val('');
    $('.searchInput').attr('placeholder', 'Enter your prefered location');
    selection = 'location'
})

$('.skill').click(() => {
    $('.experience,.location').css('border-bottom', 'none');
    $('.skill').css('border-bottom', '4px solid #E56553');
    $('.searchInput').val('');
    $('.searchInput').attr('placeholder', 'Enter your skills');
    selection = 'skills'
})

let dataWithoutEndDate = [];
$('.findButton').click(() => {
    dataWithoutEndDate.length = 0;
    if ($('.searchInput').val().length > 0) {
        $.ajax({
            type: 'GET',
            url: 'https://nut-case.s3.amazonaws.com/jobs.json',
            success: (data) => {
                localStorage.setItem('total', data.data.length);
                if (selection !== 'experience')
                    filterData(data.data, selection, $('.searchInput').val().toLowerCase());
                else {
                    let experienceData = data.data.filter(data => {
                        if (data.experience.includes($('.searchInput').val())) {
                            let value = data.experience.split('-').map(data => {
                                return parseInt(data) == $('.searchInput').val() ? true : false;
                            })
                            if (value.includes(true))
                                return data
                        }
                    })
                    filterData(experienceData, selection, $('.searchInput').val().toLowerCase());
                }
            }
        })
    } else {
        alert('Please enter a valid input');
    }
})

let filterData = (data, selection, userInput) => {
    let filtered = data.filter(data => {
        if (data[selection].toLowerCase().includes(userInput)) {
            if (data.enddate !== '')
                return data;
            dataWithoutEndDate.push(data);
        }
    }).sort((a, b) => new Date(a.enddate) - new Date(b.enddate));

    filtered = filtered.concat(dataWithoutEndDate);
    localStorage.setItem('data', JSON.stringify(filtered));
    window.location.href = 'result.html';
}