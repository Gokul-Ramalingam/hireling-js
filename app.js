$(document).ready(() => {
    $('.loginContainer').hide();
})

function func() {
    $('.formContainer').hide();
    $('.loginContainer').show();
}

function func1() {
    $('.loginContainer').hide();
    $('.formContainer').show();
}

function signup() {
    window.location.href = 'mainpage.html'
}

$("#signup").click(() => {
    let dataString = {}
    dataString.username = $('#username').val();
    dataString.email = $('#email').val();
    dataString.password = $('#password').val();
    // let dataString = 'username=' + username + '&email=' + email + "&password=" + password;
    $.ajax({
        type: "POST",
        data: JSON.stringify(dataString),
        contentType: "application/json",
        url: "http://localhost:3000/api/auth/register",
        success: (data) => {
            window.location.href = 'mainpage.html'
        },
        error: () => {
            console.log('process error');
        },
    });
})

$("#login").click(() => {
    let dataString = {}
    dataString.email = $('#lemail').val();
    dataString.password = $('#lpassword').val();
    $.ajax({
        type: "POST",
        data: JSON.stringify(dataString),
        contentType: "application/json",
        url: "http://localhost:3000/api/auth/login",
        success: (data) => {
            window.location.href = 'mainpage.html'
        },
        error: () => {
            console.log('process error');
        },
    });
})