let array = JSON.parse(localStorage.getItem("data"));
$('.found').text(array.length);
$('.total').text(localStorage.getItem('total'));

$(document).ready(() => {
  pagination(1);
});

var pagecount = array.length;

var limit = 10;
numberOfPages = Math.ceil(pagecount / limit);

$(".pagination").append(
  `<li class="page-item"><a class="page-link" href="#next">>></a></li>`
);


let currentPage;
let pagination = page => {
  currentPage = page;
  var total = page * limit;
  for (let i = total - limit; i < total && i < pagecount; i++) {
    let content = ` <div class="content">
            <div class="datas">
            <b class="companyName">${array[i].companyname.trim()}</b><br>
            <span class="profile">${
              array[i].title.length > 50
                ? array[i].title.substr(
                    0,
                    array[i].title.substr(0, 50).lastIndexOf(" ")
                  )
                : array[i].title
            }</span><br> 
            <i class="fa fa-map-marker map">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cities"> ${
              array[i].location
            }</span></i><br>
            <i class="fa fa-graduation-cap grad">&nbsp;&nbsp;<span class="skill">
            ${
              array[i].skills === ""
                ? "Not Specified"
                : array[i].skills.length > 100
                ? array[i].skills.substr(
                    0,
                    array[i].skills.substr(0, 100).lastIndexOf(" ")
                  ) + "..."
                : array[i].skills
            }</span></i><br>
            <i class="fa fa-suitcase experience">&nbsp;&nbsp;&nbsp;
            <span class="experienceInYears">
                    ${
                      array[i].experience !== ""
                        ? array[i].experience
                        : "Not Specified"
                    }</span></i><br>
                    <i class="fa fa-calendar calender">&nbsp;&nbsp;&nbsp;
                    <span class="lastDate"> ${array[i].enddate !== ''?
                    new Date(array[i].enddate).getDate()+"/"+
                    (new Date(array[i].enddate).getMonth()+1)+"/"+new Date(array[i].enddate).getFullYear():'Not Specified'
                  }
                    </span></i>
                    </div>
            <div class="action">
            <button type="button" class="btn btn-primary applyButton">View & Apply</button>
            <i class="fa fa-bookmark bookmark"></i>
        </div>`;
    $(".main").append(content);
  }
};

let paginationOnPrevNext = page => {
  if (page === "prev" && currentPage > 1) {
    currentPage--;
    window.location.hash = currentPage;
  } else if (page === "next" && currentPage < numberOfPages) {
    currentPage++;
    window.location.hash = currentPage;
  } else {
    window.location.hash = currentPage;
  }
};

$(window).on("hashchange", function () {
  $(".content").hide();
  let page = location.hash.slice(1);
  page === "prev" || page === "next" ?
    paginationOnPrevNext(page) :
    pagination(page);
});



$('.sort').click(() => {
  window.location.href = 'sortandfilter.html#sort';
})

$('.filter').click(() => {
  window.location.href = 'sortandfilter.html#filter';
})


$('.search').click(() => {
  window.location.href = 'mainpage.html';
})