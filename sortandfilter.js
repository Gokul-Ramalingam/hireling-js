$(document).ready(() => {
    let page = location.hash.slice(1);
    if (page === 'sort') {
        $('.sort').show();
        $('.filter').hide();
    } else {
        $('.filter').show();
        $('.sort').hide();
    }
})

let sortByLocation = JSON.parse(localStorage.getItem("data")).filter(arr => arr.location !== '')
    .map(arr => {
        return arr.location = '' && arr.location.includes(',') ?
            arr.location.split(',') : arr.location.includes('/') ? arr.location.split('/') : arr.location;
    })

// sortByLocation.flat()    //ES2019 method
sortByLocation = [].concat(...sortByLocation).map(arr => {
    return arr.split(',')
});

sortByLocation = [].concat(...sortByLocation).map(arr => {
    return arr.trim();
});
sortByLocation = [...new Set(sortByLocation)]
var count = 1;
sortByLocation.forEach(val => {
    $('.locationList').append(`<li class='locations' onclick=clickListener(${count})>${val}</li>`);
    count++;
})
let selection;
let clickListener = (value) => {
    $('.searchInput').val(sortByLocation[value - 1]);
    selection = 'location';
}

let experienceSorter = (value) => {
    $('.searchInput').val(value);
    selection = 'experience';
}


let filterByCompany = JSON.parse(localStorage.getItem('data'))
    .map(arr => {
        return arr.companyname;
    })

filterByCompany = [...new Set(filterByCompany)];
count = 1;
filterByCompany.forEach(company => {
    $('.companyList').append(`<li class='company' onclick=companyClickListener(${count})>${company}</li>`);
    count++;
})

let companyClickListener = (value) => {
    $('.searchInput').val(filterByCompany[value - 1]);
    selection = 'companyname';
}


$('.findButton').click(() => {
    if ($('.searchInput').val().length > 0) {

        // console.log(data.data[930].skills);
        localStorage.setItem('total', JSON.parse(localStorage.getItem("data")).length);
        filterData(JSON.parse(localStorage.getItem("data")), selection, $('.searchInput').val().toLowerCase());
    } else {
        alert('Please enter a valid input');
    }
})

let filterData = (data, selection, userInput) => {
    let filtered = data.filter(data => {
        if (data[selection].toLowerCase().includes(userInput))
            return data;
    }).sort((a, b) => new Date(a.timestamp) - new Date(b.timestamp));
    localStorage.setItem('data', JSON.stringify(filtered));
    window.location.href = 'result.html';
}