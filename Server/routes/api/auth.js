const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jsonwt = require('jsonwebtoken');
const passport = require('passport');
const key = require('../../setup/connection');

const User = require('../../models/User')

// @type           GET 
// @route          /api/auth
// @desc           Making tests
// @access         PUBLIC
router.get('/', (req, res) => res.json({
    test: 'Auth is being tested'
}));

// @type            POST
// @route          /api/auth
// @desc          route for user  registration 
// @access       PRIVATE
router.post('/register', (req, res) => {
    User.findOne({
            email: req.body.email
        })
        .then(user => {
            if (user)
                return res.status(400).json({
                    exist: 'User already exists'
                })
            else {
                const newUser = new User({
                    username: req.body.username,
                    email: req.body.email,
                    password: req.body.password
                });
                //Encrypt password using bcrypt
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        // Store hash in your password DB.
                        if (err) throw err;
                        newUser.password = hash;
                        newUser.save()
                            .then(user => res.json(user))
                            .catch(err => console.log(err))
                    });
                })

            }
        })
        .catch(err => console.log('Error  occured while checking email for availability ' + err))
})


// @type           Post 
// @route          /api/auth/login
// @desc           route for user login
// @access       PUBLIC

router.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({
        email
    }).then(user => {
        if (!user)
            return res.status(404).send({
                notExist: 'User with provided email does not exist'
            });
        bcrypt.compare(password, user.password)
            .then(isCorrect => {
                if (isCorrect) {
                    const payload = {
                        id: user.id,
                        username: user.username,
                        email: user.email
                    }

                    jsonwt.sign(
                        payload,
                        key.secret, {
                            expiresIn: 10800
                        },
                        (err, token) => {
                            if (err) throw err;
                            res.json({
                                success: true,
                                token: "Bearer " + token
                            })
                        }
                    )
                }
            })
    })
})



module.exports = router;