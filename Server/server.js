const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const passport = require('passport');
const cors = require('cors');

//routes
const auth = require('./routes/api/auth');

const app = express();

//Middleware
app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json());
app.use(cors());

const port = process.env.PORT || 3000


//mongoDB Configuration
const db = require('./setup/connection').mongoURL;


//Attempt to connect database
mongoose.connect(db,{useNewUrlParser:true,useUnifiedTopology:true})
.then(()=>console.log('Connection with MongoDB established successfully!'))
.catch(err=>console.log('Error occured while trying to connect with mongodb '+err));

//Passport middleware
app.use(passport.initialize())


//Jwt configuration
require('./strategies/jsonJwtStrategy')(passport)

//Testing route
app.get('/',(req,res)=>{
    res.send("Hey there Hireling");
    });

//Main routes
app.use('/api/auth',auth)

app.listen(port,()=>console.log(`App is running at port ${port}`));